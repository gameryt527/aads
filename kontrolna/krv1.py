import random

def quicksort(array):
    # Базовий випадок: якщо масив порожній або містить один елемент, повернути його самого
    if len(array) <= 1:
        return array
    else:
        # Вибір серединного елемента масиву як опорного
        pivot = array[len(array) // 2]

        # Створення списку лівих елементів, які менші за опорний
        left = [x for x in array if x < pivot]

        # Створення списку середніх елементів, які рівні опорному
        middle = [x for x in array if x == pivot]

        # Створення списку правих елементів, які більші за опорний
        right = [x for x in array if x > pivot]

        # Рекурсивний виклик quicksort для лівих та правих списків
        # і злиття відсортованих списків разом із середнім
        return quicksort(left) + middle + quicksort(right)

array = []
for i in range(25):
    array.append(random.randint(1, 100))

print("Початковий масив:", array)
sortedArr = quicksort(array)
print("Відсортований масив:", sortedArr)
