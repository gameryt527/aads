﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <chrono>
#include <iostream>

#define GETTIME std::chrono::steady_clock::now
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>

double func1(double n) {
	return n;
}

double func2(double n) {
	return log(n);
}

double func3(double n) {
	return n * log(n);
}

double func4(double n) {
	return pow(n, 2);
}

double func5(double n) {
	return pow(2, n);
}

double func6(double n) {
	if (n == 0) {
		return 1;
	}
	else {
		return n * func6(n - 1);
	}
}

int funcZad1(int a, int b) {
	return pow(a, b);
}

int funcZad3(int arrey[], int lenght) {
	int maxNumber = (int)-INFINITY;
	for (int i = 0; i < lenght; i++) {
		if (arrey[i] > maxNumber) {
			maxNumber = arrey[i];
		}
	}

	return maxNumber;
}


int main() {

	auto begin = GETTIME();
	for (int i = 0; i <= 50; i++) {
		printf("[%d] [f(n) = n] %.2f\n", i, func1(i));
		//printf("%.2f\n", func1(i));
	}
	auto finish = GETTIME();
	auto elapsed_ns = CALCTIME(finish - begin).count();
	printf("The time: %lld ms\n\n", elapsed_ns);



	begin = GETTIME();
	for (int i = 0; i <= 50; i++) {
		printf("[%d] [f(n) = log(n)] %.2f\n", i, func2(i));
		//printf("%.2f\n", func2(i));
	}
	finish = GETTIME();
	elapsed_ns = CALCTIME(finish - begin).count();
	printf("The time: %lld ms\n\n", elapsed_ns);



	begin = GETTIME();
	for (int i = 0; i <= 50; i++) {
		printf("[%d] [f(n) = n * log(n)] %.2f\n", i, func3(i));
		//printf("%.2f\n", func3(i));
	}
	finish = GETTIME();
	elapsed_ns = CALCTIME(finish - begin).count();
	printf("The time: %lld ms\n\n", elapsed_ns);
	


	begin = GETTIME();
	for (int i = 0; i <= 50; i++) {
		printf("[%d] [f(n) = n^2] %.2f\n", i, func4(i));
		//printf("%.2f\n", func4(i));
	}
	finish = GETTIME();
	elapsed_ns = CALCTIME(finish - begin).count();
	printf("The time: %lld ms\n\n", elapsed_ns);



	begin = GETTIME();
	for (int i = 0; i <= 50; i++) {
		printf("[%d] [f(n) = 2^n] %.2f\n", i, func5(i));
		//printf("%.2f\n", func5(i));
	}
	finish = GETTIME();
	elapsed_ns = CALCTIME(finish - begin).count();
	printf("The time: %lld ms\n\n", elapsed_ns);



	begin = GETTIME();
	for (int i = 0; i <= 50; i++) {
		printf("[%d] [f(n) = n!] %.2f\n", i, func6(i));
		//printf("%.2f\n", func6(i));
	}
	finish = GETTIME();
	elapsed_ns = CALCTIME(finish - begin).count();
	printf("The time: %lld ms\n\n", elapsed_ns);
	


	begin = GETTIME();
	srand(time(NULL));
	for (int i = 0; i <= 50; i++) {
		int a = rand() % 11;
		int b = rand() % 20;
		printf("[%d] [a^b]: %d^%d = %d\n", i, a, b, funcZad1(a, b));
	}
	finish = GETTIME();
	elapsed_ns = CALCTIME(finish - begin).count();
	printf("The time: %lld ms\n\n", elapsed_ns);



	begin = GETTIME();
	for (int i = 0; i <= 50; i++) {
		int arrey[10];
		if (i < 10) {
			printf("[0%d] [max number in array] ", i);
		}
		else {
			printf("[%d] [max number in array] ", i);

		}

		for (int i = 0; i < 10; i++) {
			arrey[i] = (rand() % 90) + 10;
			printf("%d ", arrey[i]);
		}
		printf("| max number = %d\n", funcZad3(arrey, 10));
	}
	finish = GETTIME();
	elapsed_ns = CALCTIME(finish - begin).count();
	printf("The time: %lld ms\n", elapsed_ns);

}