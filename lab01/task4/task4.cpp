﻿#include <stdio.h>
#include <windows.h>

union FloatData {
    float value;
    unsigned char bytes[4];
};


void displayBits(unsigned int value, int bitsCount) {
    unsigned int displayMask = 1 << (bitsCount - 1);

    for (int c = 1; c <= bitsCount; ++c) {
        putchar(value & displayMask ? '1' : '0');
        value <<= 1;

        if (c % 8 == 0 && c != bitsCount) {
            putchar(' ');
        }
    }

    putchar('\n');
}


void displayBytes(union FloatData data) {
    for (int i = 0; i < 4; ++i) {
        printf("Байт %d = %u\n", i, data.bytes[i]);
    }
}


void displayFloatDetails(union FloatData data) {
    unsigned int sign = (data.bytes[3] & 0x80) >> 7;
    unsigned int exponent = ((data.bytes[3] & 0x7F) << 1) | (data.bytes[2] & 0x80) >> 7;
    unsigned int mantissa = ((data.bytes[2] & 0x7F) << 16) | (data.bytes[1] << 8) | data.bytes[0];

    printf("Знак(1 - мінус, 0 - плюс): %u\n", sign);
    printf("Ступінь (8 бітів): ");
    displayBits(exponent, 8);
    printf("Мантиса (23 біта): ");
    displayBits(mantissa, 23);
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    union FloatData data;

    printf("Введіть значення типу float: "); scanf_s("%f", &data.value);

    printf("Число: %f\n\n", data.value);
    printf("Побітово:\n");
    displayBits(*(unsigned int*)&data.value, 32);
    printf("\nПобайтово:\n");
    displayBytes(data);
    printf("\nFloat (знак, ступінь, мантиса):\n");
    displayFloatDetails(data);

    printf("\nОбсяг пам'яті яку займає тип float: %lu байт\n", sizeof(data.value));
    return 0;
}
