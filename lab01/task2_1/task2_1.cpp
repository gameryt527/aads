﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

typedef struct {
    unsigned short value : 15;
    unsigned short sign : 1;
} SignedShort;

typedef union {
    signed short number;
    SignedShort parts;
} SignedShortUnion;

int main() {
    SignedShortUnion ssu;
    printf("Enter value of type signed short: ");
    scanf("%hd", &ssu.number);

    if (ssu.parts.sign) {
        unsigned short absoluteValue = ((~ssu.parts.value) & 0x7FFF) + 1;
        printf("Sign: -\n");
        printf("Value: %u\n", absoluteValue);
    }
    else {
        printf("Sign: +\n");
        printf("Value: %u\n", ssu.parts.value);
    }
}

