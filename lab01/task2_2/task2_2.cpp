﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main() {
    signed short number;
    unsigned short absoluteValue;

    printf("Enter value of type signed short: ");
    scanf("%hd", &number);

    if (number & 0x8000) {
        absoluteValue = ((~number) + 1) & 0x7FFF;
        printf("Sign: -\n");
    }
    else {
        absoluteValue = number;
        printf("Sign: +\n");
    }

    printf("Value: %u\n", absoluteValue);

    return 0;
}
