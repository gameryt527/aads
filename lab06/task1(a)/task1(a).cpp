﻿#include <iostream>
#include <algorithm> 
#include <ctime>     
#include <cstdlib>   
#include <chrono>    

void heapify(int arr[], int n, int i) {
    int largest = i; // Ініціалізація найбільшого елементу як кореня
    int left = 2 * i + 1; // лівий = 2*i + 1
    int right = 2 * i + 2; // правий = 2*i + 2

    // Якщо лівий дочірній елемент більший за корінь
    if (left < n && arr[left] > arr[largest])
        largest = left;

    // Якщо правий дочірній елемент більший, ніж найбільший до цього
    if (right < n && arr[right] > arr[largest])
        largest = right;

    // Якщо найбільший не є коренем
    if (largest != i) {
        std::swap(arr[i], arr[largest]);
        // Рекурсивно просіюємо впливовий піддерево
        heapify(arr, n, largest);
    }
}

// основна функція для сортування масиву даних типу float
void heapSort(int arr[], int n) {
    // Побудова купи (перегрупувати масив)
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify(arr, n, i);

    // Один за одним виймати елементи з купи
    for (int i = n - 1; i >= 0; i--) {
        std::swap(arr[0], arr[i]); // Поточний корінь переміщуємо в кінець
        heapify(arr, i, 0); // викликаємо процес просіювання на зменшеному купі
    }
}

// Функція для генерації випадкових float чисел у заданому діапазоні
void generateRandomInt(int arr[], int n) {
    for (int i = 0; i < n; i++) {
        arr[i] = rand() % 101; // Генерація випадкового числа в діапазоні від 0 до 100
    }
}

int main() {
    srand(time(NULL));

    int size = 10; // Розміри масивів
    int* data = new int[size]; // Створюємо масив заданого розміру

    generateRandomInt(data, size); // Генеруємо випадкові дані для масиву

    std::cout << "No sorted array:\n";
    for (int i = 0; i < size; i++) {
        std::cout << data[i] << " ";
    }
    std::cout << std::endl;

    heapSort(data, size); // Сортуємо масив

    std::cout << "\nSorted array:\n";
    for (int i = 0; i < size; i++) {
        std::cout << data[i] << " ";
    }
    std::cout << std::endl;

    delete[] data; // Звільняємо пам'ять, щоб уникнути витоку

    return 0;
}
