﻿#include <iostream>
#include <vector>
#include <chrono>

void countingSort(short arr[], int n) {
    int range = 211;
    int offset = 200; 
    std::vector<int> count(range, 0); 

    for (int i = 0; i < n; i++) {
        count[arr[i] + offset]++; 
    }

    int index = 0; 
    for (int i = 0; i < range; i++) {
        while (count[i] > 0) {
            arr[index++] = i - offset; 
            count[i]--;
        }
    }
}


void generateRandomShorts(short arr[], int n) {
    for (int i = 0; i < n; i++) {
        arr[i] = rand() % 211 - 200; // [-200, 10]
    }
}

int main() {
    srand(time(NULL));

    int sizes[] = { 10, 100, 500, 1000, 2000, 5000, 10000 }; 
    int numSizes = sizeof(sizes) / sizeof(sizes[0]); 

    for (int i = 0; i < numSizes; i++) {
        int size = sizes[i]; 
        short* data = new short[size]; 

        generateRandomShorts(data, size);

        auto start = std::chrono::high_resolution_clock::now(); // Початок вимірювання часу
        countingSort(data, size); // Сортуємо масив
        auto end = std::chrono::high_resolution_clock::now(); // Кінець вимірювання часу

        std::chrono::duration<double, std::milli> duration = end - start; // Тривалість в мілісекундах
        std::cout << "Sorting " << size << " elements took: " << duration.count() << " milliseconds." << std::endl;

    }

    return 0;
}
