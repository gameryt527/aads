﻿#include <iostream>
#include <cmath>     
#include <vector>    
#include <chrono>

void shellSort(std::vector<double>& arr) {
    int n = arr.size();
    std::vector<int> incs;
    int s = 0;
    int h = 1;

    while (h < n) {
        incs.push_back(h);
        s++;
        h = (1 << s) + 1;
    }

    for (int i = incs.size() - 1; i >= 0; i--) {
        h = incs[i];
        for (int j = h; j < n; j++) {
            double temp = arr[j];
            int k;
            for (k = j; k >= h && arr[k - h] > temp; k -= h) {
                arr[k] = arr[k - h];
            }
            arr[k] = temp;
        }
    }
}


void generateRandomDoubles(std::vector<double>& arr) {
    for (double& val : arr) {
        val = static_cast<double>(rand() % 321 - 20); // [-20, 300]
    }
}

int main() {
    srand(time(NULL));

    int sizes[] = { 10, 100, 500, 1000, 2000, 5000, 10000 }; 
    int numSizes = sizeof(sizes) / sizeof(sizes[0]); 

    for (int i = 0; i < numSizes; i++) {
        int size = sizes[i]; 
        std::vector<double> data(size); 

        generateRandomDoubles(data);

        auto start = std::chrono::high_resolution_clock::now(); 
        shellSort(data); 
        auto end = std::chrono::high_resolution_clock::now(); 

        std::chrono::duration<double, std::milli> duration = end - start; 
        std::cout << "Sorting " << size << " elements took: " << duration.count() << " milliseconds." << std::endl;
    }

    return 0;
}
