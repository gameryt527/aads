﻿#include <iostream>
#include <vector>    // Для зберігання масиву та інтервалів
#include <chrono>    // Для функцій часу
#include <cstdlib>   // Для rand() та srand()
#include <ctime>     // Для time()

void shellSort(std::vector<double>& arr) {
    int n = arr.size();
    std::vector<int> incs;
    int s = 0;
    int h = 1;

    while (h < n) {
        incs.push_back(h);
        s++;
        h = (1 << s) + 1; 
    }

    for (int i = incs.size() - 1; i >= 0; i--) {
        h = incs[i];
        for (int j = h; j < n; j++) {
            double temp = arr[j];
            int k;
            for (k = j; k >= h && arr[k - h] > temp; k -= h) {
                arr[k] = arr[k - h];
            }
            arr[k] = temp;
        }
    }
}


// Функція для генерації випадкових double чисел у заданому діапазоні
void generateRandomDoubles(std::vector<double>& arr) {
    for (double& val : arr) {
        val = static_cast<double>(rand() % 321 - 20); // Генерація випадкового числа в діапазоні від -20 до 300
    }
}

int main() {
    srand(static_cast<unsigned>(time(nullptr)));

    int size = 10; // Розміри масиву
    std::vector<double> data(size); // Створюємо вектор заданого розміру

    generateRandomDoubles(data); // Генеруємо випадкові дані для вектору
    std::cout << "Original array:\n";
    for (double el : data) {
        std::cout << el << " ";
    }
    std::cout << "\n";

    shellSort(data); // Сортуємо вектор
    std::cout << "Sorted array:\n";
    for (double el : data) {
        std::cout << el << " ";
    }
    std::cout << "\n";

    return 0;
}