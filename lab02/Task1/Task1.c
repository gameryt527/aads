#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <time.h>
#include <math.h>

#define A 1103515245                      // �������
#define C 12345                           // ������
#define M ((unsigned long long)2 << 32)   // ������
#define RANGE 100                         // ������ ���� �������� �����
#define N_VALUES 10000                    // ʳ������ �������, �� ������� �����������

// ������� ��� ��������� ���������������� �����
void generate_random_sequence(int* sequence, int length) {
    unsigned long long x = time(NULL);
    for (int i = 0; i < length; i++) {
        x = (A * x + C);
        sequence[i] = ((unsigned int)(x / M) % RANGE);
    }
}

// ������� ��� ���������� ������� ����� �����
void calculate_frequency(int* sequence, int length, int* frequency) {
    for (int i = 0; i < RANGE; i++) {
        frequency[i] = 0;
    }
    for (int i = 0; i < length; i++) {
        frequency[sequence[i]]++;
    }
}

// ����������� ����������� ���������� ����� ���������� �������
void calculate_probability(int* frequency, int length, double* probability) {
    for (int i = 0; i < RANGE; i++) {
        probability[i] = (double)frequency[i] / length;
    }
}

// ����������� ����������� ��������� ���������� �������
double calculate_expectation(double* probability, int range) {
    double expectation = 0.0;
    for (int i = 0; i < range; i++) {
        expectation += i * probability[i];
    }
    return expectation;
}

// ����������� �������� ���������� �������
double calculate_variance(double* probability, int range, double expectation) {
    double variance = 0.0;
    for (int i = 0; i < range; i++) {
        variance += probability[i] * (i - expectation) * (i - expectation);
    }
    return variance;
}

// ����������� ������������������� ��������� ���������� �������
double calculate_standard_deviation(double variance) {
    return sqrt(variance);
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int* random_sequence = malloc(N_VALUES * sizeof(int));
    int* frequency = malloc(RANGE * sizeof(int));
    double* probability = malloc(RANGE * sizeof(double));
    

    if (!random_sequence || !frequency) {
        printf("������� �������� ���'��\n");
        return 1;
    }

    generate_random_sequence(random_sequence, N_VALUES);
    calculate_frequency(random_sequence, N_VALUES, frequency);
    calculate_probability(frequency, N_VALUES, probability);
    double expectation = calculate_expectation(probability, RANGE);
    double variance = calculate_variance(probability, RANGE, expectation);
    double standardDeviation = calculate_standard_deviation(variance);

    for (int i = 0; i < N_VALUES; i++)
    {
        printf("�%d = %d\n", i, random_sequence[i]);
    }

    // ���� ������ ��� ��������
    for (int i = 0; i < RANGE; i++) {
        printf("����� %d ����������� %d ����\n", i, frequency[i]);
    }
    
    // ���� ����������� ��� ��������
    for (int i = 0; i < RANGE; i++) {
        printf("���������� ����� %d = %.4f\n", i, probability[i]);
    }

    printf("\n����������� ��������� = %.4f\n", expectation);
    printf("�������� = %.4f\n", variance);
    printf("������������������� ��������� = %.4f\n", standardDeviation);

    free(random_sequence);
    free(frequency);
    free(probability);

    return 0;
}

