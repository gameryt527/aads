﻿#include <iostream>
#include <chrono>
#include <ctime> 

// Функція сортування вставками
void insertionSort(int arr[], int n) {
    for (int i = 1; i < n; i++) {
        int key = arr[i];
        int j = i - 1;

        while (j >= 0 && arr[j] > key) {
            arr[j + 1] = arr[j];
            j--;
        }
        arr[j + 1] = key;
    }
}

// Функція для генерації масиву випадкових чисел
void generateRandomData(int arr[], int n) {
    for (int i = 0; i < n; i++) {
        arr[i] = rand() % 10000;  // Генеруємо числа в діапазоні від 0 до 9999
    }
}

int main() {
    srand(time(NULL));  // Ініціалізуємо генератор випадкових чисел з поточним часом

    int sizes[] = { 10, 100, 500, 1000, 2000, 5000, 10000 }; // Розміри масивів
    int numSizes = sizeof(sizes) / sizeof(sizes[0]); // Кількість розмірів

    for (int i = 0; i < numSizes; i++) {
        int size = sizes[i]; // Поточний розмір масиву
        int* data = new int[size]; // Створюємо масив заданого розміру

        generateRandomData(data, size);

        auto start = std::chrono::high_resolution_clock::now(); // Початок вимірювання часу
        insertionSort(data, size); // Сортуємо масив
        auto end = std::chrono::high_resolution_clock::now(); // Кінець вимірювання часу

        std::chrono::duration<double, std::milli> duration = end - start; // Тривалість в мілісекундах
        std::cout << "Sorting " << size << " elements took: " << duration.count() << " milliseconds." << std::endl;

        delete[] data;
    }

    return 0;
}
