﻿#include <stdio.h>
#include <stdlib.h>

typedef struct Node {
    int data;           // Дані, що містяться в вузлі
    struct Node* next;  // Вказівник на наступний вузол
    struct Node* prev;  // Вказівник на попередній вузол
} Node;

Node* createNode(int data) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    newNode->data = data;
    newNode->next = NULL;
    newNode->prev = NULL;
    return newNode;  // Повертаємо вказівник на новий вузол
}

void appendNode(Node** head, int data) {
    Node* newNode = createNode(data);
    if (*head == NULL) {  
        *head = newNode;  // Робимо новий вузол головою списку
        return;
    }
    Node* temp = *head;
    while (temp->next != NULL) { 
        temp = temp->next;
    }
    temp->next = newNode; 
    newNode->prev = temp;
}

void insertionSort(Node** head) {
    if (*head == NULL || (*head)->next == NULL) {
        return; 
    }
    Node* sorted = NULL;
    Node* current = *head;

    while (current != NULL) {
        Node* next = current->next; 
        current->prev = current->next = NULL; 

        if (sorted == NULL || sorted->data >= current->data) {
            current->next = sorted;
            if (sorted != NULL) {
                sorted->prev = current;
            }
            sorted = current;
        }
        else {
            Node* temp = sorted;
            while (temp->next != NULL && temp->next->data < current->data) {
                temp = temp->next;
            }
            current->next = temp->next;
            if (temp->next != NULL) {
                temp->next->prev = current;
            }
            temp->next = current;
            current->prev = temp;
        }
        current = next;
    }
    *head = sorted;
}

void printList(Node* head) {
    Node* temp = head;
    while (temp != NULL) {
        printf("%d ", temp->data);
        temp = temp->next;
    }
    printf("\n");
}

int main() {
    Node* head = NULL;
    appendNode(&head, 3);
    appendNode(&head, 1);
    appendNode(&head, 4);
    appendNode(&head, 2);

    printf("Before sorting: ");
    printList(head);

    insertionSort(&head);

    printf("After sorting: ");
    printList(head);

    return 0;
}
