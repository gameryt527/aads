﻿#include <stdio.h>
#include <stdlib.h>

typedef struct Node {
    int data;
    struct Node* next;
    struct Node* prev;
} Node;

typedef struct {
    Node* head;
    Node* tail;
} DoublyLinkedList;


// Cтворення списку
DoublyLinkedList* createList() { 
    DoublyLinkedList* list = (DoublyLinkedList*)malloc(sizeof(DoublyLinkedList));
    list->head = NULL;
    list->tail = NULL;
    return list;
}

// Функції додавання елементів в початок
void addFirst(DoublyLinkedList* list, int value) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    newNode->data = value;
    newNode->next = list->head;
    newNode->prev = NULL;

    if (list->head != NULL) {
        list->head->prev = newNode;
    }
    else {
        list->tail = newNode;
    }
    list->head = newNode;
}

// Функції додавання елементів в кінець
void addLast(DoublyLinkedList* list, int value) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    newNode->data = value;
    newNode->next = NULL;
    newNode->prev = list->tail;

    if (list->tail != NULL) {
        list->tail->next = newNode;
    }
    else {
        list->head = newNode;
    }
    list->tail = newNode;
}

// Функції додавання елементів після певного елемента
void addAfterNode(DoublyLinkedList* list, Node* prevNode, int value) {
    if (prevNode == NULL) {
        printf("Previous node cannot be NULL\n");
        return;
    }

    Node* newNode = (Node*)malloc(sizeof(Node));
    newNode->data = value;
    newNode->next = prevNode->next;
    newNode->prev = prevNode;

    if (prevNode->next != NULL) {
        prevNode->next->prev = newNode;
    }
    else {
        list->tail = newNode;
    }
    prevNode->next = newNode;
}

// Функції видалення елементів з початку
void removeFirst(DoublyLinkedList* list) {
    if (list->head == NULL) {
        printf("List is empty\n");
        return;
    }

    Node* temp = list->head;
    list->head = list->head->next;

    if (list->head != NULL) {
        list->head->prev = NULL;
    }
    else {
        list->tail = NULL;
    }

    free(temp);
}

// Функції видалення елементів з кінця
void removeLast(DoublyLinkedList* list) {
    if (list->tail == NULL) {
        printf("List is empty\n");
        return;
    }

    Node* temp = list->tail;
    list->tail = list->tail->prev;

    if (list->tail != NULL) {
        list->tail->next = NULL;
    }
    else {
        list->head = NULL;
    }

    free(temp);
}

// Функції видалення елементів після певного елемента
void removeAfterNode(DoublyLinkedList* list, Node* prevNode) {
    if (prevNode == NULL || prevNode->next == NULL) {
        printf("There is no node to remove after the given node\n");
        return;
    }

    Node* temp = prevNode->next;
    prevNode->next = temp->next;

    if (temp->next != NULL) {
        temp->next->prev = prevNode;
    }
    else {
        list->tail = prevNode;
    }

    free(temp);
}

// Функції доступу до елементів (Отримання першого і останнього елемента)
int getFirst(DoublyLinkedList* list) {
    if (list->head == NULL) {
        printf("List is empty\n");
        return -1;
    }
    return list->head->data;
}

int getLast(DoublyLinkedList* list) {
    if (list->tail == NULL) {
        printf("List is empty\n");
        return -1;
    }
    return list->tail->data;
}

// Перевірка на порожнечу
int isEmpty(DoublyLinkedList* list) {
    return list->head == NULL;
}


int main() {
    DoublyLinkedList* list = createList();

    addFirst(list, 10);
    addLast(list, 30);

    addAfterNode(list, list->head, 25);
    printf("Add elements after a specific element: %d\n", list->head->next->data);

    removeAfterNode(list, list->head);
    printf("Element after the first node after removal: %d\n", list->head->next->data);

    printf("First element: %d\n", getFirst(list));
    printf("Last element: %d\n", getLast(list));

    removeFirst(list);
    printf("First element after removal: %d\n", getFirst(list));

    removeLast(list);
    printf("Last element after removal: %d\n", getLast(list));

    printf("Is the list empty? %s\n", isEmpty(list) ? "Yes" : "No");

    while (!isEmpty(list)) { removeFirst(list); }
    free(list);

    return 0;
}

