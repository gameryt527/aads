﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

typedef struct {
    double* data;
    int top;
    int capacity;
} Stack;

void initStack(Stack* s) {
    s->capacity = 10;
    s->top = -1;
    s->data = (double*)malloc(s->capacity * sizeof(double));
}

void freeStack(Stack* s) {
    free(s->data);
    s->data = NULL;
    s->top = -1;
    s->capacity = 0;
}

int isFull(Stack* s) {
    return s->top == s->capacity - 1;
}

int isEmpty(Stack* s) {
    return s->top == -1;
}

void push(Stack* s, double value) {
    if (isFull(s)) {
        s->capacity *= 2;
        s->data = (double*)realloc(s->data, s->capacity * sizeof(double));
    }
    s->data[++s->top] = value;
}

double pop(Stack* s) {
    if (!isEmpty(s)) {
        return s->data[s->top--];
    }
    return 0.0;
}

double compute(double op1, double op2, char operator) {
    switch (operator) {
    case '+': return op1 + op2;
    case '-': return op1 - op2;
    case '*': return op1 * op2;
    case '/': return op1 / op2;
    case '^': return pow(op1, op2);
    default:  return 0.0;
    }
}

int main() {
    char expression[256];
    char* token;
    double result, op1, op2;

    while (1) {
        printf("Enter the RPN expression (e.g., '3 3 +'): ");
        fgets(expression, sizeof(expression), stdin);
        expression[strcspn(expression, "\n")] = 0;

        Stack s;
        initStack(&s);

        token = strtok(expression, " ");
        while (token != NULL) {
            if (strchr("+-*/^", token[0]) != NULL && strlen(token) == 1) {
                if (s.top < 1) {
                    printf("Error: insufficient values in the expression.\n");
                    freeStack(&s);
                    return -1;
                }
                op2 = pop(&s);
                op1 = pop(&s);
                result = compute(op1, op2, token[0]);
                push(&s, result);
            }
            else if (strcmp(token, "sqrt") == 0) {
                if (isEmpty(&s)) {
                    printf("Error: no value available for sqrt.\n");
                    freeStack(&s);
                    return -1;
                }
                op1 = pop(&s);
                result = sqrt(op1);
                push(&s, result);
            }
            else {
                push(&s, atof(token));
            }
            token = strtok(NULL, " ");
        }

        if (!isEmpty(&s)) {
            result = pop(&s);
            printf("Result: %f\n", result);
        }
        else {
            printf("Error: no result to display.\n");
        }

        freeStack(&s);
    }
    return 0;
}
