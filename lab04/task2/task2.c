﻿#include <stdio.h>
#include <stdlib.h>

typedef struct Node {
    int data;
    struct Node* next;
    struct Node* prev;
} Node;

typedef struct {
    Node* head;
    Node* tail;
} DoublyLinkedList;

DoublyLinkedList* createList() {
    DoublyLinkedList* list = (DoublyLinkedList*)malloc(sizeof(DoublyLinkedList));
    list->head = NULL;
    list->tail = NULL;
    printf("A new list has been created.\n");
    return list;
}

void addNode(DoublyLinkedList* list, int value) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    newNode->data = value;
    newNode->next = NULL;
    newNode->prev = list->tail;

    if (list->tail != NULL) {
        list->tail->next = newNode;
    }
    else {
        list->head = newNode;
    }
    list->tail = newNode;
    printf("Added %d to the list.\n", value);
}

void removeNode(DoublyLinkedList* list, int value) {
    Node* current = list->head;
    while (current != NULL) {
        if (current->data == value) {
            if (current->prev != NULL) {
                current->prev->next = current->next;
            }
            else {
                list->head = current->next;
            }
            if (current->next != NULL) {
                current->next->prev = current->prev;
            }
            else {
                list->tail = current->prev;
            }
            free(current);
            printf("Removed %d from the list.\n", value);
            return;
        }
        current = current->next;
    }
    printf("Value %d not found in the list.\n", value);
}

void printList(DoublyLinkedList* list) {
    Node* current = list->head;
    if (current == NULL) {
        printf("The list is empty.\n");
        return;
    }
    printf("List: ");
    while (current != NULL) {
        printf("%d ", current->data);
        current = current->next;
    }
    printf("\n");
}

void destroyList(DoublyLinkedList* list) {
    Node* current = list->head;
    Node* next;
    while (current != NULL) {
        next = current->next;
        free(current);
        current = next;
    }
    free(list);
    printf("The list has been destroyed.\n");
}

int main() {
    DoublyLinkedList* list = createList();
    int choice, value;

    do {
        printf("\n1. Add Element\n2. Remove Element\n3. Display List\n4. Destroy List\n5. Exit\nEnter your choice: ");
        scanf_s("%d", &choice);
        printf("\n");
        switch (choice) {
        case 1:
            printf("Enter the value to add: ");
            scanf_s("%d", &value);
            addNode(list, value);
            break;
        case 2:
            printf("Enter the value to remove: ");
            scanf_s("%d", &value);
            removeNode(list, value);
            break;
        case 3:
            printList(list);
            break;
        case 4:
            destroyList(list);
            list = createList();
            break;
        case 5:
            destroyList(list);
            printf("Exiting the program.\n");
            return 0;
        default:
            printf("Invalid choice. Please try again.\n");
        }
    } while (choice != 5);

    return 0;
}
